<?php
/**
 * Template Name: Publications Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['register_publications'] = Timber::get_posts([
	'post_type' => 'publication',
	'posts_per_page' => 3,
	'orderby' => 'date',
	'order' => 'DESC',
	'tax_query' => [
		[
			'taxonomy' => 'pub-type',
			'field' => 'slug',
			'terms' => 'the-register',
		],
	]
]);

$context['faith_publications'] = Timber::get_posts([
	'post_type' => 'publication',
	'posts_per_page' => 3,
	'orderby' => 'date',
	'order' => 'DESC',
	'tax_query' => [
		[
			'taxonomy' => 'pub-type',
			'field' => 'slug',
			'terms' => 'faith',
		],
	]
]);

$templates = [ 'archives/archive-publication.twig' ];

Timber::render( $templates, $context );