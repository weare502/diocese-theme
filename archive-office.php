<?php
/**
 * Template Name: Offices Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$post    = Timber::get_post();
$context = Timber::get_context();

$context['post'] = $post;

// get all post parents so we can get a top level count
$parents = Timber::get_posts([
	'order'          => 'ASC',
	'orderby'        => 'title',
	'post_type'      => 'office',
	'post_parent'    => 0,
	'posts_per_page' => -1,
]);

// get offices posts for building menu out
$context['cols']    = ceil( count( $parents ) / 3 );
$context['offices'] = Timber::get_posts([
	'order'          => 'ASC',
	'orderby'        => 'title',
	'post_type'      => 'office',
	'posts_per_page' => -1,
]);

$templates = [ 'archives/archive-office.twig' ];

Timber::render( $templates, $context );
