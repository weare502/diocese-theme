<?php
/**
 * Template Name: Home Template
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// get 3 most recent news articles
$context['recent_news'] = Timber::get_posts([
	'post_type' => 'news',
	'posts_per_page' => 3,
	'orderby' => 'date',
	'order' => 'DESC'
]);

$context['recent_events'] = Timber::get_posts([
	'post_type' => 'event',
	'posts_per_page' => 3,
	'orderby' => 'date',
	'order' => 'ASC'
]);


$templates = [ 'front-page.twig' ];

Timber::render( $templates, $context );