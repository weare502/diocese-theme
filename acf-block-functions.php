<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block_type/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	/** Blocks **/
	$dropdown_block = [
		'name' => 'dropdown-block',
		'title' => __( 'Dropdown Block', 'sdc' ),
		'description' => __( 'Creates a dropdown container; The content is folded into the dropdown title.', 'sdc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'sdc-blocks',
		'align' => 'center',
		'icon' => 'sort',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'drop', 'down', 'dropdown', 'accordion' ]
	];
	acf_register_block_type( $dropdown_block );

	$important_note = [
		'name' => 'important-note',
		'title' => __( 'Important Note Block', 'sdc' ),
		'description' => __( 'Creates a block with a light grey block-style note and text to the right.', 'sdc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'sdc-blocks',
		'align' => 'wide',
		'icon' => 'info',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'important', 'info', 'message', 'purple' ]
	];
	acf_register_block_type( $important_note );

	// white button, blue hover
	$solid_white_button = [
		'name' => 'solid-white-button',
		'title' => __( 'Solid White Button', 'sdc' ),
		'description' => __( 'Creates a white background button with blue text.', 'sdc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'sdc-blocks',
		'align' => 'center',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'white', 'solid' ]
	];
	acf_register_block_type( $solid_white_button );

	// bordered purple block, white background
	$solid_blue_button = [
		'name' => 'solid-blue-button',
		'title' => __( 'Solid Blue Button', 'sdc' ),
		'description' => __( 'Creates a blue background button with white text.', 'sdc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'sdc-blocks',
		'align' => 'center',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'blue', 'solid' ]
	];
	acf_register_block_type( $solid_blue_button );

	// solid mint button, reverse of bordered
	$blue_border_button = [
		'name' => 'blue-border-button',
		'title' => __( 'Blue-bordered Button', 'sdc' ),
		'description' => __( 'Creates a button with a white background, blue border and blue text.', 'sdc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'sdc-blocks',
		'align' => 'center',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'blue', 'border' ]
	];
	acf_register_block_type( $blue_border_button );

	// resources block (also used on Offices as static el)
	$resource_block = [
		'name' => 'resource-block',
		'title' => __( 'Resources Block', 'sdc' ),
		'description' => __( 'Creates a resource block with even items placed in the left column and odd items in the right column. Left to Right.', 'sdc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'sdc-blocks',
		'align' => 'center',
		'icon' => 'list-view',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'resource', 'list', 'links', 'upload' ]
	];
	acf_register_block_type( $resource_block );
endif;