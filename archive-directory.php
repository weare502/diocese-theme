<?php
/**
 * Template Name: Directory Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context         = Timber::get_context();
$post            = Timber::get_post();
$context['post'] = $post;

$emps = Timber::get_posts(
	array(
		'post_type'      => 'directory',
		'posts_per_page' => -1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'facetwp'        => true,
	)
);

foreach ( $emps as $emp ) {
	$emp->last_name = $emp->last_name ?? end( explode( ' ', $emp->post_title ) );
}

usort(
	$emps,
	function ( $a, $b ) {
		return strcmp( $a->last_name, $b->last_name );
	}
);

$context['directory_listings'] = $emps;

$templates = array( 'archives/archive-directory.twig' );

Timber::render( $templates, $context );
