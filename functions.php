<?php
// namespace prefix: sdc

// change 'views' directory to 'templates'
Timber::$locations = __DIR__ . '/templates';

class DioceseSite extends TimberSite
{

    function __construct()
    {
        // Action Hooks //
        add_action('after_setup_theme', [$this, 'after_setup_theme']);
        add_action('after_setup_theme', [$this, 'sdc_color_palette']);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);
        add_action('admin_head', [$this, 'admin_head_css']);
        add_action('admin_menu', [$this, 'admin_menu_cleanup']);
        add_action('init', [$this, 'register_post_types']);
        add_action('acf/init', [$this, 'render_custom_acf_blocks']);
        add_action('login_enqueue_scripts', [$this, 'style_login']);
        add_action('init', [$this, 'shared_taxonomies']);

        // Filter Hooks //
        add_filter('timber_context', [$this, 'add_to_context']);
        add_filter('gform_enable_field_label_visibility_settings', [$this, '__return_true']);
        add_filter('block_categories', [$this, 'sdc_block_category'], 10, 2);

        // Comment Column Removal //
        add_filter('manage_edit-page_columns', [$this, 'disable_admin_columns']);
        add_filter('plugins_auto_update_enabled', [$this, '__return_false']); // remove useless (flywheel controls updates)

        require 'custom-post-types/page-for-post-type.php';
        add_action('after_setup_theme', ['Page_For_Post_Type', 'get_instance']);

        parent::__construct();
    }

    // hide admin area annoyances
    function admin_head_css()
    {
        ?>
        <style type="text/css">
          #wp-admin-bar-comments {
            display: none !important;
          }

          .update-nag {
            display: none !important;
          }

          .mwp-notice-container {
            display: none !important;
          }

          #auto_post_redirection {
            display: none !important;
          }

          /* rankmath annoyance */

          /* move cpt's into more visible group */
          .menu-icon-office {
            margin-top: 1rem;
          }

          .menu-icon-publication {
            margin-bottom: 0.75rem;
          }
        </style>
        <?php
    }

    // WP admin login styles
    function style_login()
    {
        ?>
        <style type="text/css">
          #login h1, .login h1 {
            background-color: #fff;
            padding: 0.75rem 0.5rem;
            border-radius: 2px;
          }

          #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/static/images/sdc-logo.png);
            background-position: center;
            width: 9rem;
            height: 3rem;
            background-size: contain;
            padding: 1rem 4rem;
            margin: 0 auto;
          }
        </style>
        <?php
    }

    function enqueue_scripts()
    {
        $version = filemtime(get_stylesheet_directory() . '/style-dist.css');
        wp_enqueue_style('fullcalendar-css', get_stylesheet_directory_uri() . '/static/css/main.min.css', [], $version);
        wp_enqueue_style('sdc-css', get_stylesheet_directory_uri() . '/style-dist.css', [], $version);
        wp_enqueue_style('sdc-block-css', get_stylesheet_directory_uri() . '/block-style-dist.css', [], $version);

        wp_enqueue_script('wp-api');
        wp_enqueue_script('moment-js', get_template_directory_uri() . '/static/js/moment.js', ['jquery'], $version);
        wp_enqueue_script('fullcalendar-js', get_template_directory_uri() . '/static/js/main.min.js', ['jquery'], $version);
        wp_enqueue_script('sdc-js', get_template_directory_uri() . '/static/js/site.js', ['jquery'], $version);
        wp_enqueue_script('map-app-js', get_template_directory_uri() . '/static/js/map-app.js', ['jquery'], $version);

        // register mapbox api and enqueue it (only for Parishes and Schools pages)
        if (is_page(90) or is_page(93)) {
            wp_register_script('mapbox', 'https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js');
            wp_enqueue_script('mapbox');
        }
    }

    // Custom Timber context helper functions
    function add_to_context($context)
    {
        $context['site'] = $this;
        $context['date'] = date('F j, Y');
        $context['date_year'] = date('Y');
        $context['options'] = get_fields('option');
        $context['home_url'] = home_url('/');
        $context['is_front_page'] = is_front_page();
        $context['get_url'] = $_SERVER['REQUEST_URI'];
        $context['events_url'] = get_permalink(get_option('page_for_event'));

        return $context;
    }

    // Menus / Theme Support / ACF Options Page
    function after_setup_theme()
    {
        register_nav_menu('mini-top', 'Top Navigation');
        register_nav_menu('primary', 'Site Navigation');
        register_nav_menu('footer-quick-links', 'Footer Quick Links');

        add_theme_support('menus');
        add_theme_support('align-wide');
        add_theme_support('post-thumbnails');
        add_theme_support('editor-styles');
        add_editor_style('block-style-dist.css');

        if (function_exists('acf_add_options_page')) {
            acf_add_options_page([
                    'page_title' => 'Global Site Data',
                    'menu_title' => 'Global Site Data',
                    'capability' => 'edit_posts',
                    'redirect' => false,
                    'updated_message' => 'Global Options Updated!',
                ]
            );
        }
    }

    // registers and renders our custom acf blocks
    function render_custom_acf_blocks()
    {
        require 'acf-block-functions.php';
    }

    // creates a custom category for our theme-specific blocks
    function sdc_block_category($categories, $post)
    {
        return array_merge(
            $categories, [
                [
                    'slug' => 'sdc-blocks',
                    'title' => '502 Blocks',
                ],
            ]
        );
    }

    // get rid of clutter
    function disable_admin_columns($columns)
    {
        unset($columns['comments']);
        return $columns;
    }

    function admin_menu_cleanup()
    {
        remove_menu_page('edit.php'); // Posts
        remove_menu_page('edit-comments.php'); // Comments
    }

    function sdc_color_palette()
    {
        add_theme_support('disable-custom-colors');
        $colors = [
            [
                // Blue Color
                'name' => __('Blue', 'sdc'),
                'slug' => 'blue',
                'color' => '#0A396B',
            ],

            [
                // Gold Color
                'name' => __('Gold', 'sdc'),
                'slug' => 'gold',
                'color' => '#BF9F70',
            ],

            [
                // Light Black Color (Grey)
                'name' => __('Light Black', 'sdc'),
                'slug' => 'grey',
                'color' => '#303030',
            ],
        ];
        add_theme_support('editor-color-palette', $colors);
    }

    // add cpts here
    function register_post_types()
    {
        include_once 'custom-post-types/post-type-office.php';
        include_once 'custom-post-types/post-type-parish.php';
        include_once 'custom-post-types/post-type-school.php';
        include_once 'custom-post-types/post-type-directory.php';
        include_once 'custom-post-types/post-type-event.php';
        include_once 'custom-post-types/post-type-news.php';
        include_once 'custom-post-types/post-type-publication.php';
    }

    // register shared taxonomies here
    function shared_taxonomies()
    {
        // Tax for linking Blog/Events/Directory posts to Offices posts
        $tax_labels = [
            'name' => _x('Offices', 'sdc'),
            'singular_name' => _x('Office', 'sdc'),
            'search_items' => __('Search Offices', 'sdc'),
            'all_items' => __('All Offices', 'sdc'),
            'edit_item' => __('Edit Office', 'sdc'),
            'update_item' => __('Update Office', 'sdc'),
            'add_new_item' => __('Add Office', 'sdc'),
            'new_item_name' => __('Create Office', 'sdc'),
            'menu_name' => __('Offices', 'sdc'),
            'parent_item' => null,
        ];

        $tax_args = [
            'hierarchical' => true,
            'labels' => $tax_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => false,
            'has_archive' => false,
            'query_var' => true,
            'show_in_rest' => true,
            'rewrite' => true,
        ];
        register_taxonomy('offices', ['event', 'news', 'directory'], $tax_args);

        $tax_labels = [
            'name' => _x('Parishes', 'sdc'),
            'singular_name' => _x('Parish', 'sdc'),
            'search_items' => __('Search Parishes', 'sdc'),
            'all_items' => __('All Parishes', 'sdc'),
            'edit_item' => __('Edit Parish', 'sdc'),
            'update_item' => __('Update Parish', 'sdc'),
            'add_new_item' => __('Add Parish', 'sdc'),
            'new_item_name' => __('Create Parish', 'sdc'),
            'menu_name' => __('Parishes', 'sdc'),
            'parent_item' => null,
        ];

        $tax_args = [
            'hierarchical' => true,
            'labels' => $tax_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'has_archive' => false,
            'show_in_nav_menus' => false,
            'query_var' => true,
            'show_in_rest' => true,
            'rewrite' => true,
        ];
        register_taxonomy('parishes', ['event', 'news', 'directory'], $tax_args);

        $tax_labels = [
            'name' => _x('Schools', 'sdc'),
            'singular_name' => _x('School', 'sdc'),
            'search_items' => __('Search Schools', 'sdc'),
            'all_items' => __('All Schools', 'sdc'),
            'edit_item' => __('Edit School', 'sdc'),
            'update_item' => __('Update School', 'sdc'),
            'add_new_item' => __('Add School', 'sdc'),
            'new_item_name' => __('Create School', 'sdc'),
            'menu_name' => __('Schools', 'sdc'),
            'parent_item' => null,
        ];

        $tax_args = [
            'hierarchical' => true,
            'labels' => $tax_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'has_archive' => false,
            'show_in_nav_menus' => false,
            'query_var' => true,
            'show_in_rest' => true,
            'rewrite' => true,
        ];
        register_taxonomy('schools', ['event', 'news', 'directory'], $tax_args);
    }
} // End of DioceseSite class

new DioceseSite();

// main site nav
function sdc_render_primary_menu()
{
    wp_nav_menu([
        'theme_location' => 'primary',
        'container' => false,
        'menu_id' => 'primary-menu',
    ]);
}

// mini top nav
function sdc_render_top_nav()
{
    wp_nav_menu([
        'theme_location' => 'mini-top',
        'container' => false,
        'menu_id' => 'mini-top-menu',
    ]);
}

// footer quick links
function sdc_render_footer_quick_links()
{
    wp_nav_menu([
        'theme_location' => 'footer-quick-links',
        'container' => false,
        'menu_id' => 'footer-quick-links',
    ]);
}

// run if  _wp_page_template  is not empty (custom template is used)
// for the Default Template it will be empty. (default is used when no template is set)
// Code Courtesy of: Bill Erickson
function ea_disable_editor($id = false)
{
    $excluded_templates = [
        'front-page.php',
        'archive-publication.php',
    ];

    if (empty($id)) {
        return false;
    }

    $id = intval($id);
    $template = get_page_template_slug($id);

    return in_array($template, $excluded_templates);
}

function ea_disable_gutenberg($can_edit, $post_type)
{
    if (!(is_admin() && !empty($_GET['post']))) {
        return $can_edit;
    }

    if (ea_disable_editor($_GET['post'])) {
        $can_edit = false;
    }

    return $can_edit;
}

add_filter('gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2);
add_filter('use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2);

// move our ACF Options Page (Global Site Data) below the Dashboard tab
function custom_menu_order($menu_ord)
{
    if (!$menu_ord) {
        return true;
    }

    $menu = 'acf-options-global-site-data';

    // remove from current menu
    $menu_ord = array_diff($menu_ord, array($menu));

    // append after index [0]
    array_splice($menu_ord, 1, 0, array($menu));

    return $menu_ord;
}

add_filter('custom_menu_order', 'custom_menu_order');
add_filter('menu_order', 'custom_menu_order');

// hide editor on certain template pages
function hide_editor()
{
    // $_REQUEST holds both GET and POST variables.
    $post_id = !empty($_REQUEST['post']) ? $_REQUEST['post'] : null; // phpcs:ignore WordPress.Security.NonceVerification.Recommended

    if (empty($post_id)) {
        return;
    }

    $template_file = get_post_meta($post_id, '_wp_page_template', true);

    // no editor on these templates
    if ('archive-publication.php' === $template_file) {
        remove_post_type_support('page', 'editor');
    }
}

add_action('admin_init', 'hide_editor');

add_filter(
    'facetwp_is_main_query',
    function ($is_main_query, $query) {
        if (is_post_type_archive('event')) {
            if (empty($query->get('facetwp'))) {
                $is_main_query = false;
            }
        }
        return $is_main_query;
    }, 10, 2
);

/**
 * Get related posts of a specific post type for a given post.
 *
 * @param string $post_type
 * @param object $post
 * @return array|bool|null
 * @since Oct 12, 2020
 *
 * @author Daron Spence <daronspence@gmail.com>
 */
function sdc_get_related_posts_of_type(string $post_type, $post, ?int $posts_per_page = null)
{
    // $key[$value] where $key is the post type and $value is the taxonomy slug.
    $taxonomy_lookup_table = [
        'office' => 'offices',
        'school' => 'schools',
        'parish' => 'parishes',
        'news' => 'news',
        'event' => 'event',
        'directory' => 'directory'
    ];

    if (empty($posts_per_page)) {
        $posts_per_page = get_option('posts_per_page', 10);
    }

    $posts = Timber\Timber::get_posts(
        apply_filters(
            'sdc_get_related_posts_of_type_args', [
            'post_type' => $post_type,
            'posts_per_page' => $posts_per_page,
            'tax_query' => [
                [
                    'taxonomy' => $taxonomy_lookup_table[$post->post_type],
                    'field' => 'slug',
                    'terms' => ['all', $post->post_name],
                ],
            ],
        ],
            $post_type,
            $post,
            $posts_per_page,
            $taxonomy_lookup_table
        )
    );
    return $posts;
}

add_filter('sdc_get_related_posts_of_type_args', 'sdc_filter_related_events', 10, 2);
/**
 * Modify the related posts query to only grab upcoming events and sort them by date/time.
 *
 * @param array $query
 * @param string $post_type
 * @return array
 * @since Oct 12, 2020
 *
 * @author Daron Spence <daronspence@gmail.com>
 */
function sdc_filter_related_events(array $query, string $post_type): array
{
    if ('event' !== $post_type) {
        return $query;
    }

    $query['orderby'] = [
        'event_date' => 'ASC',
        'event_time' => 'ASC',
    ];

    $query['meta_query'] = [
        'relation' => 'AND',
        'event_date' => [
            'key' => 'event_date',
            'compare' => '>=',
            'value' => wp_date('Y-m-d'),
            'type' => 'DATE',
        ],
        'event_time' => [
            'key' => 'event_time',
            'type' => 'TIME',
        ],
    ];
    return $query;
}

// remove facet counts
add_filter('facetwp_facet_html', function ($output, $params) {
    if ('dropdown' == $params['facet']['type']) {
        $output = preg_replace("/( \([0-9]+\))/m", '', $output);
    }
    return $output;
}, 10, 2);

function facet_dropdown_add_class()
{ ?>
    <script>
      (function ($) {
        $(document).on('facetwp-loaded', function () {
          $('.facetwp-dropdown').each(function () {
            if (this.value == "") {
              $(this).parent().removeClass('active');
            } else {
              $(this).parent().addClass('active');

            }
          });
        });
      })(jQuery);
    </script>
    <?php
}

add_action('wp_head', 'facet_dropdown_add_class', 100);


add_filter('facetwp_search_query_args', function ($args) {
    if (is_post_type_archive('event')) {
        $args['orderby'] = array(
            'event_date' => 'ASC',
            'event_time' => 'ASC',
        );
        $args['meta_query'] = array(
            'relation' => 'AND',
            'event_date' => array(
                'key' => 'event_date',
                'compare' => '>=',
                'value' => wp_date('Y-m-d'),
                'type' => 'DATE',
            ),
            'event_time' => array(
                'key' => 'event_time',
                'type' => 'TIME',
            ),
        );
    }
    return $args;
});

function search_filter($query) {
    if ( !is_admin() && $query->is_main_query() ) {
        if ($query->is_search) {
            $query->set('posts_per_page', '-1');
        }
    }

}

add_action('pre_get_posts','search_filter');

///Search query with acf fields
/* Join posts and post-meta tables
*
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );

////////


function acf_to_rest_api($response, $post, $request)
{
    if (!function_exists('get_fields')) return $response;

    if (isset($post)) {
        $acf = get_fields($post->id);
        $response->data['acf'] = $acf;
    }
    return $response;
}

add_filter('rest_prepare_event', 'acf_to_rest_api', 10, 3);


add_filter( 'rest_event_query', 'se35728943_change_post_per_page', 10, 2 );

function se35728943_change_post_per_page( $args, $request ) {
    $max = max( (int) $request->get_param( 'custom_per_page' ), 200 );
    $args['posts_per_page'] = $max;
    return $args;
}

add_image_size( 'author-size', 100, 100,  true );


/**
 * Filter work post type by schools slug
 *
 * @param array $args
 * @param WP_Rest_Rquest $request
 * @return array $args
 */
function filter_rest_event_schools( $args, $request ) {
    $params = $request->get_params();
    if(isset($params['global_school_filter'])){
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'schools',
                'field' => 'slug',
                'terms' => explode(',', $params['global_school_filter'])
            )
        );
    }
    return $args;
}
// add the filter
add_filter( "rest_event_query", 'filter_rest_event_schools', 10, 2 );


/**
 * Filter work post type by parishes slug
 *
 * @param array $args
 * @param WP_Rest_Rquest $request
 * @return array $args
 */
function filter_rest_event_parishes( $args, $request ) {
    $params = $request->get_params();
    if(isset($params['global_parish_filter'])){
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'parishes',
                'field' => 'slug',
                'terms' => explode(',', $params['global_parish_filter'])
            )
        );
    }
    return $args;
}
// add the filter
add_filter( "rest_event_query", 'filter_rest_event_parishes', 10, 2 );


/**
 * Filter work post type by offices slug
 *
 * @param array $args
 * @param WP_Rest_Rquest $request
 * @return array $args
 */
function filter_rest_event_offices( $args, $request ) {
    $params = $request->get_params();
    if(isset($params['global_office_filter'])){
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'offices',
                'field' => 'slug',
                'terms' => explode(',', $params['global_office_filter'])
            )
        );
    }
    return $args;
}
// add the filter
add_filter( "rest_event_query", 'filter_rest_event_offices', 10, 2 );
