<?php

use Timber\Post;
use Timber\Timber;
/**
 * Template Name: News Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post    = Timber::get_post(get_option('page_for_news')); // phpcs:ignore

/** @var Post $post */
$context['post'] = $post;

$context['featured']    = $post->meta( 'featured_news' );

if($context['featured']) :
	$context['featured_xl'] = array_shift( $context['featured'] );
endif;

$templates = array( 'archives/archive-news.twig' );

Timber::render( $templates, $context );
