<?php

use Timber\Timber;
/**
 * Template Name: Schools Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context            = Timber::get_context();
$post               = Timber::get_post(); // phpcs:ignore
$context['post']    = $post;

// Delte the above crap and uncomment these lines when schools are uploaded.
$context['schools'] = Timber::get_posts(
	array(
		'post_type'      => 'school',
		'posts_per_page' => -1,
	)
);

$templates = array( 'archives/archive-school.twig' );

Timber::render( $templates, $context );
