const SCHOOLS_CSV_LINK = 'https://docs.google.com/spreadsheets/d/1heHZvSeBoSYK-gW19Ibmub9YNPfsWLEvWYgSij3IVEs/gviz/tq?tqx=out:csv'
const config = {
  initMapMilesPadding: 50,
  viewInfoText: 'View School Info',
  viewInfoLinkKey: 'School Link',
  defaultMarkerColor: '#3FB1CE',
  selectedMarkerColor: '#0A396B',
  selectedRowColor: '#e6f5ff',
  selectedRowZoom: 10,
  hoverMarkerColor: '#0A396B',
  geocoderFillColor: '#088',
};
// map functions below
$(document).ready(function () {
  $.ajax({
    type: "GET",
    url: SCHOOLS_CSV_LINK,
    dataType: "text",
    success: function (csvData) {
      csv2geojson.csv2geojson(csvData, {
        latfield: 'Latitude',
        lonfield: 'Longitude',
        delimiter: ','
      }, function (err, geojson) {
        var mapApp = new MapApp(geojson, config);
        mapApp.run();
      });
    }
  });
});