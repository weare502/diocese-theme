const PARISH_CSV_LINK = 'https://docs.google.com/spreadsheets/d/1QDtAQ2Jm4zXsNa0rLwrPk69LZsGeamVg_vncYQ_VOZw/gviz/tq?tqx=out:csv&sheet=sheet1'
const config = {
  initMapMilesPadding: 100,
  viewInfoText: 'View Parish Info',
  viewInfoLinkKey: 'Parish Link',
  defaultMarkerColor: '#3FB1CE',
  selectedMarkerColor: '#0A396B',
  selectedRowColor: '#e6f5ff',
  selectedRowZoom: 10,
  hoverMarkerColor: '#0A396B',
  geocoderFillColor: '#088',
};
// map functions below
$(document).ready(function () {
  $.ajax({
    type: "GET",
    url: PARISH_CSV_LINK,
    dataType: "text",
    success: function (csvData) {
      csv2geojson.csv2geojson(csvData, {
        latfield: 'Latitude',
        lonfield: 'Longitude',
        delimiter: ','
      }, function (err, geojson) {
        var mapApp = new MapApp(geojson, config);
        mapApp.run();
      });
    }
  });
});