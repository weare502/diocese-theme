(function($) {
  // Needed for the maps to work.
  window.$ = jQuery

  $.urlParam = function(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href)
    if (results == null) {
      return null
    } else {
      return decodeURI(results[1]) || 0
    }
  }

  let dataEvent = {
    school: '',
    office: '',
    parish: '',
  }

  function mobileCheck() {
    if (window.innerWidth >= 768) {
      return 'dayGridMonth'
    } else {
      return 'listWeek'
    }
  }

  let eventsJson = []
  let arrg = []
  let $true = false
  var calendar

  function restEvents(dataEvent) {
    fetch(dataEvent.url)
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.length) {
          return arrg = data
        }
      }).then(data => {
      for (var i = 0; i < arrg.length; i++) {
        let date = arrg[i]
        let y = date.acf.event_date
        let x = date.acf.event_end_date

        if (y && date.acf.event_time) {
          startDate = date.acf.event_date + 'T' + date.acf.event_time
        } else {
          startDate = date.acf.event_date
        }

        if (x) {
          endDate = date.acf.event_end_date
        } else {
          endDate = date.acf.event_date;
        }

        let title = date.title.rendered.replace(/&#8211;/g, '-').replace(/&#8217;/g, '\'')
          .replace(/&#038;/g, '&')

        eventsJson.push({
          title: title,
          start: startDate,
          end: endDate,
          url: date.link,
          // allDay: data[i].acf.all_day,
          // startTime: data[i].acf.event_time,
        })
      }
       if (eventsJson.length) {

        var calendarEl = document.getElementById('calendar')
        calendar = new FullCalendar.Calendar(calendarEl, {
          // initialView: 'dayGridMonth',

          eventTimeFormat: {
            hour: 'numeric',
            minute: '2-digit',
            hour12: true,
            meridiem: 'short',
          },

          events: eventsJson,
          height: 'auto',
          headerToolbar: {
            left: 'prev title next',
            center: '',
            right: '',
          },
          displayEventTime: true,
          displayEventEnd: true,
          dayPopoverFormat: 'dddd DD/MM',
          initialView: mobileCheck(),

          windowResize: function(view) {
            if (window.innerWidth >= 768 && $true) {
              $true = false
              calendar.changeView('dayGridMonth')
            } else if (window.innerWidth < 768 && !$true) {
              $true = true
              calendar.changeView('listWeek')
            }
          },

          dayHeaderContent: (args) => {
            return moment(args.date).format('dddd')
          },

          // eventRender: function(eventObj, $el) {
          //   $el.popover({
          //     title: eventObj.title,
          //     content: eventObj.description,
          //     trigger: 'hover',
          //     html: true,
          //     placement: 'top',
          //     container: 'body',
          //   })
          // },
        })
        return calendar.render()
      }
    })
  }

  $(document).ready(function() {

    /*Button View More Publications pages*/
    $('.link.js-more').click(function() {
      if ($('.link.js-more').hasClass('active')) {
        $('.link.js-more').removeClass('active').text('View More')
        $('.js-facet .facetwp-facet').css({ 'height': 216, 'overflow': 'hidden' })
      } else {
        $('.js-facet .facetwp-facet').css({ 'height': 'auto' })
        $('.link.js-more').text('View Less').addClass('active')
      }
    })

    $('.js-button-list').click(function() {
      $('.js-event').removeClass('active')
      $(this).addClass('active')
      $('.js-wrapper-list').show()
      $('.js-wrapper-calendar').css('height', '0')
    })

    $('.js-button-calendar').click(function() {
      $('.js-event').removeClass('active')
      $(this).addClass('active')
      $('.js-wrapper-list').hide()
      $('.js-wrapper-calendar').css('height', '100%')
    })

    let $body = $('body')
    let cookie = document.cookie.replace(/(?:(?:^|.*;\s*)cds_notificationClosed\s*\=\s*([^;]*).*$)|^.*$/, '$1')

    if ($body.hasClass('home')) {
      $body.fadeIn(1250)
    } else {
      // noscript fallback if js is disabled for this
      $body.css('display', 'block')
    }

    if (!cookie) {
      $('.alert-banner').addClass('alert-banner-visible')
    }

    // navigation functions/handlers
    $(function siteNavigation() {
      // alert banner close function
      $('#close-alert').click(function() {
        // create date for cookie
        let date = new Date()
        date.setTime(date.getTime() + 7 * 24 * 60 * 60 * 1000)
        document.cookie = 'cds_notificationClosed=1; expires=' + date

        $('.alert-banner').hide()
        $('.background-underlay .top-change').css('top', '0') // reset top position on blur gradient
        $('.background-underlay-home .top-change').css('top', '0') // reset top position on blur gradient
      })

      // menu open/close functions
      $('#menu-toggle').click(function() {
        $('.x-bar').toggleClass('x-bar-active')
        $('#mini-top-menu').fadeToggle(450)
        $('#searchform').fadeToggle(450)
        $('#primary-menu').fadeToggle(450)
        $('.nav-wrapper .nav-menu').toggleClass('lock-nav')
        $('body').toggleClass('lock-body')


        // hide pge items for menu
        $('.content-wrapper').fadeToggle(200)
        $('header').fadeToggle(200)
        $('footer').fadeToggle(200)
      })
    })

    // desktop and mobile search module switcher
    if ($body.width() < 651) {
      $(function mobileSearchFilter() {
        $('.search-module').hide()
        $('#mobile-search-expand').click(function() {
          $('.search-module').slideToggle(450)
          $(this).toggleClass('color-fill')
        })
      })
    } // only on small screens

    // dropdown block - controls and aria events for screenreaders
    $(function dropDownBlock() {
      $('.dropdown-content').each(function() {
        $(this).hide()
      })

      $('.dropdown-block-title').click(function() {
        var $this = $(this)
        // fires on first click (content is expanded)
        if ($this.hasClass('target')) {
          $this.removeClass('target')
          $this.next('.dropdown-content').slideToggle(350)
          $this.attr('aria-pressed', 'true')
          $this.next('.dropdown-content').attr('aria-expanded', 'true')
        } else {
          // fires on second click (content is closed)
          $this.next('.dropdown-content:first').slideToggle(350, function() {
            $this.prev('.dropdown-block-title').addClass('target')
            $this.prev('.dropdown-block-title').attr('aria-pressed', 'false')
            $this.attr('aria-expanded', 'false')
          })
        }
        // always fire
        $(this).toggleClass('chevron-rotate')
      })
    }) // end dropdown block

    /*Facetwp search event*/
    $('body').on('keyup', '.facetwp-search', function(e) {
      if (e.keyCode == '13') {
        setTimeout(function() {
          $('.js-button-list').trigger('click')
        }, 1500)
      }
    })

    if ($.urlParam('_global_search_module') != null) {
      setTimeout(function() {
        $('.js-button-list').trigger('click')
      }, 500)
    }
    /////////////
  }) // end Document.Ready


  $(document).on('facetwp-loaded', function() {
    if ($('.js-facet .facetwp-facet').children('div').length > 5 && !$('.facetwp-radio').hasClass('checked')) {
      $('.js-facet .facetwp-facet').css({ 'height': 216, 'overflow': 'hidden' })
      $('.link.js-more').text('View More').removeClass('active')
    } else if ($('.js-facet .facetwp-facet').children('div').length > 5 && $('.facetwp-radio').hasClass('checked')) {
      $('.link.js-more').text('View Less').addClass('active')
    } else {
      $('.link.js-more').hide()
    }
    dataEvent.url = wpApiSettings.root + 'wp/v2/event?'
    let searchUrl = $.urlParam('_global_search_module'),
      schoolUrl = $.urlParam('_global_school_filter'),
      parishesUrl = $.urlParam('_global_parish_filter'),
      officesUrl = $.urlParam('_global_office_filter')

    if (searchUrl != null || schoolUrl != null || parishesUrl != null || officesUrl != null) {

      if (schoolUrl != null) {
        dataEvent.school = schoolUrl
        dataEvent.url = dataEvent.url + '&global_school_filter=' + dataEvent.school
      }
      if (searchUrl != null) {
        dataEvent.search = searchUrl
        dataEvent.url = dataEvent.url + '&search=' + dataEvent.search
      }

      if (parishesUrl != null) {
        dataEvent.parish = parishesUrl
        dataEvent.url = dataEvent.url + '&global_parish_filter=' + dataEvent.parish
      }

      if (officesUrl != null) {
        dataEvent.office = officesUrl
        dataEvent.url = dataEvent.url + '&global_office_filter=' + dataEvent.office
      }
    }

    eventsJson = []
    arrg = []
    // $('#calendar').empty()

    restEvents(dataEvent)

  })

})(jQuery)

window.switchToMap = function() {
   jQuery('.map-twig').addClass('showing-map').removeClass('showing-list')
}

window.switchToList = function() {
   jQuery('.map-twig').addClass('showing-list').removeClass('showing-map')
}
