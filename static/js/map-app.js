// Used for Ajax request to get Google Sheet Mapper Data
var transformRequest = (url, resourceType) => {
  var isMapboxRequest =
      url.slice(8, 22) === "api.mapbox.com" ||
      url.slice(10, 26) === "tiles.mapbox.com";

  return {
    url: isMapboxRequest ? url.replace("?", "?pluginName=sheetMapper&") : url
  };
};
// This will let you use the .remove() function later on
if (!('remove' in Element.prototype)) {
  Element.prototype.remove = function () {
    if (this.parentNode) {
      this.parentNode.removeChild(this);
    }
  };
}

function setMarkerColor(marker, color) {
  var $elem = jQuery(marker.getElement());
  $elem.find('svg g[fill="' + marker._color + '"]').attr('fill', color);
  marker._color = color;
}

const MAPBOX_TOKEN = 'pk.eyJ1Ijoiam9yZGFuNTAyIiwiYSI6ImNqZWVoZmQ3aTI5MWQyd28xMDZiN3R5aXcifQ.IpeRF59rJ3j7A4FPEGp5SA'; //Mapbox token
const ROW_ID_PREFIX = 'result_row';
const RESULT_ROW_CLASS = 'result_row';
const MAPBOX_STYLE = 'mapbox://styles/jordan502/ck9stnlrh06gu1jmzz0xini3x';
const geocoderCircleId = 'resultCicle';
const maxBoundsMapFactor = 2;
const defaultRadius = 5;
const defaultCircleOpacity = 0.2;
var togglePopupFromRow = false;

class MapApp {
  constructor(geojson, config) {
    this.geojson = geojson; // list of geojson from csv
    this.MAPBOX_TOKEN = MAPBOX_TOKEN;
    this.map; // mapbox map
    this.mapBbox; // data driven map bounding box
    this.markerHashTable = {}; // hash table to hold the markers
    this.config = config;
    this.geocoder = undefined;
    this.geocoderCircleSource = undefined;
    this.geocoderCircleLayer = undefined;
  }

  run() {
    this.initMap();
    var self = this;
    this.initResults(() => {
      self.initGeocoder();
      self.initMarkers();
    });
  }

  initMap() {
    var polygon = turf.buffer(turf.convex(this.geojson), this.config.initMapMilesPadding, { units: 'miles' });
    var polygonMax = turf.buffer(turf.convex(this.geojson),
        this.config.initMapMilesPadding * maxBoundsMapFactor,
        { units: 'miles' }
    );
    this.mapBbox = turf.square(turf.bbox(polygon));
    this.mapBboxMax = turf.square(turf.bbox(polygonMax));

    var self = this;
    // mapbox setup, tokens, defaults
    mapboxgl.accessToken = this.MAPBOX_TOKEN;
    self.map = new mapboxgl.Map({
      container: 'map',
      style: MAPBOX_STYLE,
      bounds: this.mapBbox,
      maxBounds: this.mapBboxMax,
      // center: [-96.186306, 39.358485],
      // zoom: 10,
      transformRequest: transformRequest,
      // scrollZoom: false
    });

    // Add zoom and rotation controls to the map.
    self.map.addControl(
        new mapboxgl.NavigationControl({
          showCompass: false,
          showZoom: true
        })
    );

    // Add geolocate control to the map.
    self.map.addControl(
        new mapboxgl.GeolocateControl({
          positionOptions: {
            enableHighAccuracy: true
          },
          trackUserLocation: true
        })
    );
  }

  initResults(cb) {
    for (var feature of this.geojson.features) {
      var row = feature.properties;
      // <li class="list-group-item active">
      // List
      $("#result_list").append(
          `<li class="list-group-item ${RESULT_ROW_CLASS}" id="${ROW_ID_PREFIX}_${row['Unique ID']}">
            <a href="${row['Parish Link'] ? row['Parish Link'] : row['School Link']}"></a>
          <div><b>${row['Category Name']}</b></div>
          <div>${row['Address']}</div>
        </li>`);
    }
    cb();
    this.initAutoComplete();
  }

  initGeocoder() {
    var self = this;
    this.geocoder = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      marker: true,
      placeholder: 'Address'
    });
    this.geocoder.setBbox(this.mapBbox);

    this.geocoder.setFlyTo(false);
    this.initGeocoderOnResult();
    this.initGeocoderOnClear();
    document.getElementById('geocoder').appendChild(this.geocoder.onAdd(self.map));
  }

  initGeocoderOnClear() {
    var self = this;
    this.geocoder.on('clear', () => {
      for (var markerId in self.markerHashTable) {
        var rowId = `${ROW_ID_PREFIX}_${markerId}`;
        $(`#${rowId}`).css('display', 'inline');
      }

      if (self.geocoderCircleSource && self.geocoderCircleLayer) {
        self.map.removeLayer(geocoderCircleId);
        self.map.removeSource(geocoderCircleId);
        self.geocoderCircleSource = undefined;
        self.geocoderCircleLayer = undefined;
      }
    });

    $('#reset-button').on('click', (e) => {
      self.geocoder.clear();
      self.resetMarkers(false);
      self.map.fitBounds(self.mapBboxMax);
    });

  }
  initGeocoderOnResult() {
    var self = this;
    this.geocoder.on('result', (result) => {
      self.resetMarkers(false);
      for (var markerId in self.markerHashTable) {
        var marker = self.markerHashTable[markerId];
        var rowId = `${ROW_ID_PREFIX}_${markerId}`;
        $(`#${rowId}`).css('display', 'inline');
      }

      var center = result.result.center;
      var selectedOption = $('.radius-option').toArray().filter(el => {
        return el.selected;
      })[0];
      var radius = parseInt(selectedOption.attributes.value.nodeValue);
      if (radius == -1) {
        radius = defaultRadius;
      }
      var options = { steps: 64, units: 'miles' };
      var circle = turf.circle(center, radius, options);
      if (self.geocoderCircleSource && self.geocoderCircleLayer) {
        self.map.removeLayer(geocoderCircleId);
        self.map.removeSource(geocoderCircleId);
      }

      self.geocoderCircleSource = self.map.addSource(geocoderCircleId, {
        'type': 'geojson',
        'data': circle
      });

      self.geocoderCircleLayer = self.map.addLayer({
        'id': geocoderCircleId,
        'type': 'fill',
        'source': geocoderCircleId,
        'layout': {},
        'paint': {
          'fill-color': self.config.geocoderFillColor,
          'fill-opacity': defaultCircleOpacity,
        }
      });

      var bbox = turf.bbox(circle);

      for (var markerId in self.markerHashTable) {
        var marker = self.markerHashTable[markerId];
        var markerPoint = turf.point([marker._lngLat.lng, marker._lngLat.lat]);
        if (!turf.booleanContains(circle, markerPoint)) {
          var rowId = `${ROW_ID_PREFIX}_${markerId}`;
          $(`#${rowId}`).css('display', 'none');
        }
      }

      self.map.fitBounds(
          bbox,
          {
            padding: 20
          }
      );
    });
  }

  initMarkers() {
    self = this;
    this.map.on('load', function () {
      for (var feature of self.geojson.features) {
        var row = feature.properties;
        var popup = new mapboxgl.Popup({ offset: 25 }).setHTML(
            `<div>
              <div class="name">${row['Category Name']}</div>  
              <div class="address">${row['Address']}</div>
              <div class="city">${row['City']}, KS,  ${row['Zip']}</div>
              <div><a href="${row[self.config.viewInfoLinkKey]}">${self.config.viewInfoText} ></a> </div>
              
            </div>`
        );
        popup.id = row['Unique ID'];
        popup.on('open', (e) => {
          var markerId = e.target.id;
          if (!togglePopupFromRow) {
            self.resetMarkers(true, markerId);
          }

          $("." + RESULT_ROW_CLASS).css("background-color", "white");
          $(`#${ROW_ID_PREFIX}_${markerId}`).css("background-color", self.config.selectedRowColor);
          let viewportWidth = window.innerWidth || document.documentElement.clientWidth;
          scrollResize();
          window.addEventListener('resize', scrollResize);
          function scrollResize(){
            if (viewportWidth > 768) {
              document.getElementById(`${ROW_ID_PREFIX}_${markerId}`).scrollIntoView();
            }
          }
          var selectedMarker = self.markerHashTable[markerId];
          setMarkerColor(selectedMarker, '#0A396B');
          togglePopupFromRow = false;
        });


        var marker = new mapboxgl.Marker({ color: self.config.defaultMarkerColor })
            .setPopup(popup)
            .setLngLat([feature.geometry.coordinates[0], feature.geometry.coordinates[1]])
            .addTo(self.map);
        marker.selected = false;
        self.markerHashTable[row['Unique ID']] = marker;
      }
    });
    this.initRowHover();
    this.initRowClick();
  }

  initRowHover() {
    var self = this;
    $("." + RESULT_ROW_CLASS).hover(
        function (e) {
          // handlerIn
          $(e.currentTarget).css("background-color", "#E3E9F1");
          var markerId = e.currentTarget.id.replace(`${ROW_ID_PREFIX}_`, '');
          var oldMarker = self.markerHashTable[markerId];
          if (!oldMarker || oldMarker.selected) {
            return;
          }
          var options = {
            color: self.config.hoverMarkerColor,
            scale: 1.2,
          };
          var newMarker = new mapboxgl.Marker(options)
              .setPopup(oldMarker.getPopup())
              .setLngLat(oldMarker.getLngLat())
              .addTo(self.map);
          oldMarker.remove();
          self.markerHashTable[markerId] = newMarker;
        },
        function (e) {
          $(e.currentTarget).css("background-color", "white");
          var markerId = e.currentTarget.id.replace(`${ROW_ID_PREFIX}_`, '');

          var oldMarker = self.markerHashTable[markerId];
          if (!oldMarker || oldMarker.selected) {
            return;
          }

          var newMarker = new mapboxgl.Marker(
          )
              .setPopup(oldMarker.getPopup())
              .setLngLat(oldMarker.getLngLat())
              .addTo(self.map);
          oldMarker.remove();
          self.markerHashTable[markerId] = newMarker;
        }
    );
  }
  initRowClick() {
    var self = this;
    $("." + RESULT_ROW_CLASS).on('click', function (e) {
      togglePopupFromRow = true;
      self.resetMarkers(false);
      e.stopPropagation();
      e.stopImmediatePropagation();
      $(e.currentTarget).css("background-color", self.config.selectedRowColor);
      var markerId = e.currentTarget.id.replace(`${ROW_ID_PREFIX}_`, '');
      var oldMarker = self.markerHashTable[markerId];
      var options = {
        color: self.config.selectedMarkerColor,
      };
      var newMarker = new mapboxgl.Marker(options)
          .setPopup(oldMarker.getPopup())
          .setLngLat(oldMarker.getLngLat())
          .addTo(self.map);
      oldMarker.remove();
      newMarker.selected = true;
      self.markerHashTable[markerId] = newMarker;
      self.map.flyTo({ center: newMarker.getLngLat(), zoom: self.config.selectedRowZoom });
      newMarker.togglePopup();
    });
  }
  resetMarkers(excludeSelected, selectedMarkerId) {
    for (var markerId in this.markerHashTable) {
      var oldMarker = this.markerHashTable[markerId];
      if (excludeSelected && selectedMarkerId == markerId) {
        continue;
      }
      var newMarker = new mapboxgl.Marker({ color: self.config.defaultMarkerColor })
          .setPopup(oldMarker.getPopup())
          .setLngLat(oldMarker.getLngLat())
          .addTo(this.map);
      oldMarker.remove();
      this.markerHashTable[markerId] = newMarker;
    }
  }
  initAutoComplete() {
    var self = this;
    // var autocomplete_input = $("#search_list_input");
    var autocomplete_input = document.getElementById("search_list_input");
    autocomplete({
      input: autocomplete_input,
      fetch: function (text, update) {
        text = text.toLowerCase();
        // you can also use AJAX requests instead of preloaded data
        // var suggestions = countries.filter(n => n.label.toLowerCase().startsWith(text))
        var suggestions = self.geojson.features.filter(n => n.properties['Category Name'].toLowerCase().includes(text));
        update(suggestions);
      },
      onSelect: function (item) {
        // item.properties['Unique ID']
        var rowId = `${ROW_ID_PREFIX}_${item.properties['Unique ID']}`;
        document.getElementById(rowId).scrollIntoView();
        $("#" + rowId).trigger('click');
      },
      render: function (item, currentValue) {
        var div = document.createElement("div");
        div.textContent = item.properties['Category Name'];
        return div;
      },
    });
  }
}
