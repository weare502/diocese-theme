<?php
/**
 * Template Name: About Template
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = [ 'about.twig' ];

Timber::render( $templates, $context );
