<?php

use Timber\Timber;
/**
 * Template Name: Parishes Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context         = Timber::get_context();
$post            = Timber::get_post(); // phpcs:ignore
$context['post'] = $post;

$context['parishes'] = Timber::get_posts(
	array(
		'post_type'      => 'parish',
		'posts_per_page' => -1,
	)
);

foreach ( $context['parishes'] as $parish ) {
	$parish->address = $parish->parish_address;
}

$templates = array( 'archives/archive-parish.twig' );

Timber::render( $templates, $context );
