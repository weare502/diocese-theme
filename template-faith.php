<?php
/**
 * Template Name: faith Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['pub_years'] = Timber::get_terms( ['taxonomies' => 'pub-year'] );

$context['faith_publications'] = Timber::get_posts([
	'post_type' => 'publication',
	'posts_per_page' => 10,
    'orderby' => array('date' => 'DESC'),
	'tax_query' => [
		'relation' => 'AND',
		[
			'taxonomy' => 'pub-type',
			'field' => 'slug',
			'terms' => 'faith',
		],
	]
]);

$templates = [ 'faith.twig' ];

Timber::render( $templates, $context );
