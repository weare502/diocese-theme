<?php

$templates = array( 'search.twig', 'archive.twig', 'index.twig' );
$context = Timber::get_context();

$context['search_query'] = get_search_query();
$context['search_title'] =  get_search_query();
$context['posts'] = new Timber\PostQuery();
$context['search_count'] = count($context['posts']);

Timber::render( $templates, $context );
