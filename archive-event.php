<?php

use Timber\Timber;
/**
 * Template Name: Events Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context             = Timber::get_context();
$post                = Timber::get_post( get_option( 'page_for_event' ) );
$context['featured'] = $post->meta( 'featured_events' );
$context['post']     = $post;
$context['events']   = Timber::get_posts(
	array(
		'facetwp'    => true,
		'post_type'  => 'event',
		// 'meta_key'   => 'event_date',
		'orderby'    => array(
			'event_date' => 'ASC',
			'event_time' => 'ASC',
		),
		// 'meta_type'  => 'DATE',
		// 'order'      => 'ASC',
		'meta_query' => array(
			'relation'   => 'AND',
			'event_date' => array(
				'key'     => 'event_date',
				'compare' => '>=',
				'value'   => wp_date( 'Y-m-d' ),
				'type'    => 'DATE',
			),
			'event_time' => array(
				'key' => 'event_time',
				'type' => 'TIME',
			),
		),
	)
);

$templates = array( 'archives/archive-event.twig' );

Timber::render( $templates, $context );
