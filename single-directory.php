<?php

use Timber\Timber;
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
*/

$context         = Timber::get_context();
$post            = Timber::get_post();
$context['post'] = $post;

// If the directory person has a WP user, get that WP user object.
$users = get_users(
	array(
		'meta_key'   => 'employee_relationship',
		'meta_value' => $post->ID,
	)
);

$author = array_shift( $users );

if ( ! empty( $author ) ) {
	$context['authored_news'] = Timber::get_posts(
		array(
			'posts_per_page' => 2,
			'post_type'      => 'news',
			'author'         => $author->ID,
		)
	);
} else {
	$context['authored_news'] = array();
}
$context['author'] = $author->ID;
the_post();

Timber::render( array( 'singles/single-directory.twig', 'singles/single.twig' ), $context );
