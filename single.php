<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// only setup for single posts
if( is_singular() ) :
	if( $post->post_parent != 0 ) {
		$context['is_child'] = true;
	} else {
		$context['is_parent'] = true;
	}
endif;

/**** OFFICES ****/
$parent_args = [
	'post_parent' => $post->ID,
	'post_type' => 'directory',
];

$context['child_posts'] = $parent_args;

// sdc_get_related_posts_of_type( string $post_type, $post, ?int $posts_per_page = null )
$context['directory'] = sdc_get_related_posts_of_type( 'directory', $post, -1 );
$context['events'] = sdc_get_related_posts_of_type( 'event', $post, 3 );
$context['news'] = sdc_get_related_posts_of_type( 'news', $post, 3 );

// setup parish related news and events
if( is_singular('parish') ) {
	$context['parish_news'] = sdc_get_related_posts_of_type( 'news', $post, 3 );
	$context['parish_events'] = sdc_get_related_posts_of_type( 'event', $post, 3 );
}

// set up schools news and related events
if( is_singular('school') ) {
	$context['school_news'] = sdc_get_related_posts_of_type( 'news', $post, 3 );
	$context['school_events'] = sdc_get_related_posts_of_type( 'event', $post, 3 );
}

if( is_singular('office') ) {
	$context['office_news'] = sdc_get_related_posts_of_type( 'news', $post, 2 );

	$context['office_directory'] = Timber::get_posts([
		'post_type' => 'directory',
		'posts_per_page' => -1,
		'meta_key' => 'last_name',
		'orderby' => 'meta_value',
		'order' => 'ASC',
		'tax_query' => [
			[
				'taxonomy' => 'offices',
				'field'    => 'slug',
				'terms'    => $post->post_title,
			],
		],
	]);
}

the_post();

Timber::render( [ 'singles/single-' . $post->ID . '.twig', 'singles/single-' . $post->post_type . '.twig', 'singles/single.twig' ], $context );