<?php
// Parishes + Archive

// setup the post type
$labels = [
	'name'               => __( 'Parishes', 'sdc' ),
	'singular_name'      => __( 'Parish', 'sdc' ),
	'add_new'            => _x( 'Add Parish', 'sdc', 'sdc' ),
	'add_new_item'       => __( 'Add Parish', 'sdc' ),
	'edit_item'          => __( 'Edit Parish', 'sdc' ),
	'new_item'           => __( 'New Parish', 'sdc' ),
	'view_item'          => __( 'View Parishes', 'sdc' ),
	'search_items'       => __( 'Search Parishes', 'sdc' ),
	'not_found'          => __( 'No Parishes found', 'sdc' ),
	'not_found_in_trash' => __( 'No Parishes found in Trash', 'sdc' ),
	'parent_item_colon'  => __( 'Parent Parish:', 'sdc' ),
	'menu_name'          => __( 'Parishes', 'sdc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-admin-multisite',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main directory page
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'editor', 'title', 'thumbnail', 'page-attributes' ],
];
register_post_type( 'parish', $args );