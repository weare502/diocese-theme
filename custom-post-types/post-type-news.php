<?php
// (blog) News/Updates - (Schools/Parishes/Offices link to this)

// setup the post type
$labels = [
	'name'               => __( 'News/Updates', 'sdc' ),
	'singular_name'      => __( 'News', 'sdc' ),
	'add_new'            => _x( 'Add Entry', 'sdc', 'sdc' ),
	'add_new_item'       => __( 'Add Entry', 'sdc' ),
	'edit_item'          => __( 'Edit Entry', 'sdc' ),
	'new_item'           => __( 'New Entry', 'sdc' ),
	'view_item'          => __( 'View Entries', 'sdc' ),
	'search_items'       => __( 'Search Entries', 'sdc' ),
	'not_found'          => __( 'No Entries found', 'sdc' ),
	'not_found_in_trash' => __( 'No Entries found in Trash', 'sdc' ),
	'parent_item_colon'  => __( 'Parent Entry:', 'sdc' ),
	'menu_name'          => __( 'News/Updates', 'sdc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [ 'offices', 'category', 'parishes', 'schools' ],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-megaphone',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main directory page
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'editor', 'title', 'thumbnail', 'author' ],
];
register_post_type( 'news', $args );