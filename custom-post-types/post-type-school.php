<?php
// Schools + Archive

// setup the post type
$labels = [
	'name'               => __( 'Schools', 'sdc' ),
	'singular_name'      => __( 'School', 'sdc' ),
	'add_new'            => _x( 'Add School', 'sdc', 'sdc' ),
	'add_new_item'       => __( 'Add School', 'sdc' ),
	'edit_item'          => __( 'Edit School', 'sdc' ),
	'new_item'           => __( 'New School', 'sdc' ),
	'view_item'          => __( 'View Schools', 'sdc' ),
	'search_items'       => __( 'Search Schools', 'sdc' ),
	'not_found'          => __( 'No Schools found', 'sdc' ),
	'not_found_in_trash' => __( 'No Schools found in Trash', 'sdc' ),
	'parent_item_colon'  => __( 'Parent School:', 'sdc' ),
	'menu_name'          => __( 'Schools', 'sdc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-welcome-learn-more',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main directory page
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'editor', 'title', 'thumbnail', 'page-attributes' ],
];
register_post_type( 'school', $args );