<?php
// Directory Pages + Archive (Schools/Parishes/Offices link to this)

// setup the post type
$labels = [
	'name'               => __( 'Directory', 'sdc' ),
	'singular_name'      => __( 'Directory', 'sdc' ),
	'add_new'            => _x( 'Add Person', 'sdc' ),
	'add_new_item'       => __( 'Add Person', 'sdc' ),
	'edit_item'          => __( 'Edit Person', 'sdc' ),
	'new_item'           => __( 'New Person', 'sdc' ),
	'view_item'          => __( 'View Directory', 'sdc' ),
	'search_items'       => __( 'Search Directory', 'sdc' ),
	'not_found'          => __( 'No People found', 'sdc' ),
	'not_found_in_trash' => __( 'No People found in Trash', 'sdc' ),
	'parent_item_colon'  => __( 'Parent Directory:', 'sdc' ),
	'menu_name'          => __( 'Directory', 'sdc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [ 'location', 'offices', 'parishes', 'schools' ],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-businessman',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main directory page
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'editor', 'title', 'thumbnail' ],
];
register_post_type( 'directory', $args );

// Directory location Tax
$tax_labels = [
	'name' 				=> _x( 'Locations', 'sdc' ),
	'singular_name' 	=> _x( 'Location', 'sdc' ),
	'search_items' 		=> __( 'Search Locations', 'sdc' ),
	'all_items' 		=> __( 'All Locations', 'sdc' ),
	'edit_item' 		=> __( 'Edit Location', 'sdc' ),
	'update_item' 		=> __( 'Update Location', 'sdc' ),
	'add_new_item' 		=> __( 'Add Location', 'sdc' ),
	'new_item_name' 	=> __( 'Create Location', 'sdc' ),
	'menu_name' 		=> __( 'Locations', 'sdc' ),
	'parent_item'		=> NULL,
];

$tax_args = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true,
	'rewrite'			=> true,
];
register_taxonomy( 'location', 'directory', $tax_args );
