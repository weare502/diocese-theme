<?php
// Event Pages + Archive - (Schools/Parishes/Offices link to this)

// setup the post type
$labels = [
	'name'               => __( 'Events', 'sdc' ),
	'singular_name'      => __( 'Event', 'sdc' ),
	'add_new'            => _x( 'Add Event', 'sdc', 'sdc' ),
	'add_new_item'       => __( 'Add Event', 'sdc' ),
	'edit_item'          => __( 'Edit Event', 'sdc' ),
	'new_item'           => __( 'New Event', 'sdc' ),
	'view_item'          => __( 'View Events', 'sdc' ),
	'search_items'       => __( 'Search Events', 'sdc' ),
	'not_found'          => __( 'No Events found', 'sdc' ),
	'not_found_in_trash' => __( 'No Events found in Trash', 'sdc' ),
	'parent_item_colon'  => __( 'Parent Event:', 'sdc' ),
	'menu_name'          => __( 'Events', 'sdc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [ 'offices', 'schools', 'parishes', 'event-type' ],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-calendar-alt',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main directory page
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'editor', 'title', 'thumbnail' ],
];
register_post_type( 'event', $args );

// Event Type Taxonomy
$tax_labels = [
	'name' 				=> _x( 'Event Types', 'sdc' ),
	'singular_name' 	=> _x( 'Event Type', 'sdc' ),
	'search_items' 		=> __( 'Search Event Types', 'sdc' ),
	'all_items' 		=> __( 'All Event Types', 'sdc' ),
	'edit_item' 		=> __( 'Edit Event Type', 'sdc' ),
	'update_item' 		=> __( 'Update Event Type', 'sdc' ),
	'add_new_item' 		=> __( 'Add Event Type', 'sdc' ),
	'new_item_name' 	=> __( 'Create Event Type', 'sdc' ),
	'menu_name' 		=> __( 'Event Types', 'sdc' ),
	'parent_item'		=> NULL,
];

$tax_args = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true,
	'rewrite'			=> true,
];
register_taxonomy( 'event-type', 'event', $tax_args );