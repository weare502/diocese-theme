<?php
// Publications + Archive

// setup the post type
$labels = [
	'name'               => __( 'Publications', 'sdc' ),
	'singular_name'      => __( 'Publication', 'sdc' ),
	'add_new'            => _x( 'Add Publication', 'sdc', 'sdc' ),
	'add_new_item'       => __( 'Add Publication', 'sdc' ),
	'edit_item'          => __( 'Edit Publication', 'sdc' ),
	'new_item'           => __( 'New Publication', 'sdc' ),
	'view_item'          => __( 'View Publications', 'sdc' ),
	'search_items'       => __( 'Search Publications', 'sdc' ),
	'not_found'          => __( 'No Publications found', 'sdc' ),
	'not_found_in_trash' => __( 'No Publications found in Trash', 'sdc' ),
	'parent_item_colon'  => __( 'Parent Publication:', 'sdc' ),
	'menu_name'          => __( 'Publications', 'sdc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => [ 'pub-type', 'pub-year' ],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-paperclip',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => false,
	'exclude_from_search' => false,
	'has_archive'         => true, // main publications page
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'thumbnail' ],
];
register_post_type( 'publication', $args );

// Publication Type Tax
$tax_labels = [
	'name' 				=> _x( 'Publication Types', 'sdc' ),
	'singular_name' 	=> _x( 'Publication Type', 'sdc' ),
	'search_items' 		=> __( 'Search Publication Types', 'sdc' ),
	'all_items' 		=> __( 'All Publication Types', 'sdc' ),
	'edit_item' 		=> __( 'Edit Publication Type', 'sdc' ),
	'update_item' 		=> __( 'Update Publication Type', 'sdc' ),
	'add_new_item' 		=> __( 'Add Publication Type', 'sdc' ),
	'new_item_name' 	=> __( 'Create Publication Type', 'sdc' ),
	'menu_name' 		=> __( 'Publication Types', 'sdc' ),
	'parent_item'		=> NULL,
];

$tax_args = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true,
	'rewrite'			=> true,
];
register_taxonomy( 'pub-type', 'publication', $tax_args );

// Publication Year Tax
$tax_labels = [
	'name' 				=> _x( 'Publication Year', 'sdc' ),
	'singular_name' 	=> _x( 'Publication Year', 'sdc' ),
	'search_items' 		=> __( 'Search Publication Years', 'sdc' ),
	'all_items' 		=> __( 'All Publication Years', 'sdc' ),
	'edit_item' 		=> __( 'Edit Publication Year', 'sdc' ),
	'update_item' 		=> __( 'Update Publication Year', 'sdc' ),
	'add_new_item' 		=> __( 'Add Publication Year', 'sdc' ),
	'new_item_name' 	=> __( 'Create Publication Year', 'sdc' ),
	'menu_name' 		=> __( 'Publication Years', 'sdc' ),
	'parent_item'		=> NULL,
];

$tax_args = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true,
	'rewrite'			=> true,
];
register_taxonomy( 'pub-year', 'publication', $tax_args );