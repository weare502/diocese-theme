<?php
// Offices + Archive

// setup the post type
$labels = [
	'name'               => __( 'Offices', 'sdc' ),
	'singular_name'      => __( 'Office', 'sdc' ),
	'add_new'            => _x( 'Add Office', 'sdc', 'sdc' ),
	'add_new_item'       => __( 'Add Office', 'sdc' ),
	'edit_item'          => __( 'Edit Office', 'sdc' ),
	'new_item'           => __( 'New Office', 'sdc' ),
	'view_item'          => __( 'View Offices', 'sdc' ),
	'search_items'       => __( 'Search Offices', 'sdc' ),
	'not_found'          => __( 'No Offices found', 'sdc' ),
	'not_found_in_trash' => __( 'No Offices found in Trash', 'sdc' ),
	'parent_item_colon'  => __( 'Parent Office:', 'sdc' ),
	'menu_name'          => __( 'Offices', 'sdc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-admin-home',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main directory page
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'editor', 'title', 'thumbnail', 'page-attributes' ],
];
register_post_type( 'office', $args );